import React from 'react';
import logo from './logo.svg';
import './App.css';
import TodoPage from './pages/TodoPage';
import UserPage from './pages/UserPage';
import LoginPage from './pages/LoginPage';

function App() {
  return (
    <div className="App">
      <LoginPage/>
      <UserPage/>
    </div>
  );
}

export default App;
