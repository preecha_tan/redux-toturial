import React from 'react'
import { Input } from 'antd';
import { Button, Radio, Icon } from 'antd';
import Header from '../components/Header'

const LoginPage =()=>{
    return(
        <center><div>
            <br/><br/><br/><br/>
            <div scope="row"><Input placeholder="Username" style={{width:'20%'}} /></div>
            <br/>
            <div scope="row"><Input placeholder="Password" style={{width:'20%'}} /></div>
            <br/>
            <Button type="primary" size={'large'} onClick={()=> window.location = "/processlogin"} >Log in</Button>
        </div></center>
    )
}

export default LoginPage