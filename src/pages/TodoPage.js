import React, { useEffect } from 'react'
import TodoTollBar from '../components/TodoToolbar'
import TodoList from '../components/TodoList'
import { useDispatch } from 'react-redux'
import { fetchTodos } from '../reducers/TodoReducer'

const TodoPage = ()=>{

    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch(fetchTodos())
    },[])

    return(
        <div>
            <TodoTollBar></TodoTollBar>
            <TodoList></TodoList>
        </div>
    )
}

export default TodoPage;