import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { fetchUsers, searchUser } from '../reducers/UserReducer';
import UserToolbar from '../components/UserToolbar';
import UserList from '../components/UserList'

const UserPage = () => {

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchUsers())
    }, [])

    return (
        <div>
            <UserToolbar/>
            <UserList />
        </div>
    );

}

export default UserPage;