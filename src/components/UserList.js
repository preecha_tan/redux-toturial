import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { fetchUsers, searchUser } from '../reducers/UserReducer';
import { useHistory } from 'react-router';
import { Button } from 'antd';

const UserList = () => {

    const UserData = useSelector(state => state.usersComp.data);
    const search = useSelector(state => state.usersComp.usersearchText);
    console.log(UserData)

    const history = useHistory()

    const onTodo = (id)=>{
        history.push("/todo/"+id)
    }


    return (
        <div>
            
            <center><h1>User List</h1><table>
                <thead>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email</th>
                </thead>
                {
                    search == '' ?
                        UserData.map((todo) => {
                            return (
                                <tbody>
                                    <tr>
                                        <td>{todo.id}</td>
                                        <td>{todo.name}</td>
                                        <td>{todo.email}</td>
                                        <td><a href={"/todo/" + todo.id}>Todo</a></td>
                                    </tr>
                                </tbody>
                            )
                        })
                        :
                        UserData.filter(m => m.name.match(search)).map((todo) => {
                            return (
                                <tbody>
                                    <tr>
                                    <td>{todo.id}</td>
                                        <td>{todo.name}</td>
                                        <td>{todo.email}</td>
                                        <td><a href={"/todo/" + todo.id }>Todo</a></td>
                                    </tr>
                                </tbody>
                            )
                        })
                }
            </table>
            <br/><br/><Button type="primary" size={'large'} onClick={()=> window.location = "/processlogout"} >Log out</Button>
            </center>
        </div>
    )

}

export default UserList;