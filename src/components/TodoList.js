import React from 'react'
import { useSelector, connect, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { doingToDone } from '../reducers/TodoReducer'
import { List, Typography } from 'antd';
import { Button } from 'antd';



const TodoList = () => {

    const todos = useSelector(state => state.todosComp.data)
    const search = useSelector(state => state.todosComp.searchText)
    const filterData = useSelector(state => state.todosComp.filterType)

    const onFilter = () => {
        if (filterData == 'All') {
            return todos
        } else {
            return todos.filter((value) => {
                return value.completed.toString() == filterData
            })
        }
    }

    const dispatch = useDispatch()

    const onDone = (id) => {
        dispatch(doingToDone(id))
    }
    console.log(todos)
    return (
        <div>
            <center><h1>To do List</h1>
            </center>
            <center><table>
                <thead>

                    <th>Id</th>
                    <th>Title</th>
                    <th>Status</th>
                </thead>
                {
                    search == '' ?
                        onFilter().map((todo, index) => {
                            return (
                                <tbody>
                                    <tr>

                                        <td>{todo.id}</td>
                                        <td>{
                                            todo.completed == false ?
                                                todo.title
                                                :
                                                <s>{todo.title}</s>
                                        }</td>
                                        <td>{
                                            todo.completed  ?
                                                "Done"
                                                :
                                                <button onClick={()=>{onDone(todo.id)}}>Doing</button>
                                        }</td>

                                    </tr>
                                </tbody>
                            )
                        })
                        :
                        onFilter().filter(m => m.title.match(search)).map((todo) => {
                            return (
                                <tbody>
                                    <tr>

                                        <td>{todo.id}</td>
                                        <td>{
                                            todo.completed == false ?
                                                todo.title
                                                :
                                                <s>{todo.title}</s>
                                        }</td>  
                                        <td>{
                                            todo.completed  ?
                                                "Done"
                                                :
                                                <button onClick={()=>{onDone(todo.id)}}>Doing</button>
                                        }</td>
                                    </tr>
                                </tbody>
                            )
                        })
                }
            </table></center>
        </div>
    )
}

// const mapStateToProps = state => {
//     return {
//         todos: state.todosComp.data,
//         search: state.todosComp.searchText,
//     }
// }

// const mapDispatchToProps = dispatch => {
//     return bindActionCreators({searchTodo,filterTodo,fetchTodos},dispatch)


// }


export default TodoList;