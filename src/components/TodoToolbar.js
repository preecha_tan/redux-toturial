import React,{useState} from 'react'
import { searchTodo, filterTodo } from '../reducers/TodoReducer'
import { useDispatch } from 'react-redux'
import { Button } from 'antd';


const TodoToolBar = ()=>{

    const [newSearch, setNewSearch] = useState('')
    const dispatch = useDispatch();

    const onSearch = (e)=>{
        let value = e.target.value
        dispatch(searchTodo(value))
        setNewSearch(value)
    }

    const onFilter = (value)=>{
        let filterChoose = value.target.value
        dispatch(filterTodo(filterChoose))
    }

    return(
        <div>
            <br/><br/>
            <center>
            <Button type="primary" size={'large'} onClick={()=> window.location = "/processlogout"} >Log out</Button><br/><br/>
            <input value={newSearch} onChange={onSearch} placeholder="Searchc list here"></input>
            <select defaultValue="All" onChange={onFilter}>
                <option value='All'>ALL</option>
                <option value={false}>DOING</option>
                <option value={true} >DONE</option>
            </select><br/><br/></center>
        </div>
    )
}

export default TodoToolBar;