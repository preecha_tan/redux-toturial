import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { fetchUsers, searchUser } from '../reducers/UserReducer';
import { List, Avatar, Button, Skeleton } from 'antd';
import { Input } from 'antd';
import reqwest from 'reqwest';

const UserToolbar = () => {

    const { Search } = Input;

    const [newSearch, setNewSearch] = useState('')

    const dispatch = useDispatch();

    const onSearch = (e) => {
        let value = e.target.value
        dispatch(searchUser(value))
        setNewSearch(value)
    }

    return (
        <div>
            <center>
                <br /><br />
                <input value={newSearch} onChange={onSearch} placeholder="Searchc user here"></input>
                <br /><br />
            </center>
        </div>
    );

}

export default UserToolbar;