import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import { rootReducer } from './reducers/index';
import thunk from 'redux-thunk'
import 'antd/dist/antd.css';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';
import Header from './components/Header';
import LoginPage from './pages/LoginPage';
import UserPage from './pages/UserPage';
import TodoPage from './pages/TodoPage';

const composeEnhancers =
    typeof window === 'object' &&
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        }) : compose;

const enhancer = composeEnhancers(
    applyMiddleware(thunk),
);

const appStore = createStore(rootReducer, enhancer)

const PrivateRoute = ({ path, component: Component }) => ( //check ว่าลอคอินรึยัง
    <Route path={path} render={(props) => {
        const isLogin = localStorage.getItem('isLogin')

        if (isLogin == 'true')
            return <Component {...props} />
        else
            return <Redirect to="/login" />
    }} />
);



ReactDOM.render(
    <Provider store={appStore}>
        <BrowserRouter>
            <Route path="/" component={LoginPage} exact={true} ></Route>
            {/* <Route path="/login" component={LoginPage} exact={true} ></Route> */}
            <Route path="/processlogin" render={() => {
                localStorage.setItem('isLogin', true);
                return <Redirect to="/users" />
            }}></Route>
            <Route path="/processlogout" render={() => {
                localStorage.setItem('isLogin', false);
                return <Redirect to="/" />
            }}></Route>

            <PrivateRoute path="/users" component={UserPage}></PrivateRoute>
            <Route path="/todo/:user_id" component={TodoPage} ></Route>
        </BrowserRouter>
    </Provider>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
