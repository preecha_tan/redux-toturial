export const SEARCH_TODO = 'SEARCH_TODO'
export const FILTER_TODO = 'FILTER_TODO'
export const FETCH_TODOS_BEGIN = 'FETCH_TODOS_BEGIN'
export const FETCH_TODOS_SUCCESS = 'FETCH_TODOS_SUCCESS'
export const FETCH_TODOS_ERROR = 'FETCH_TODOS_ERROR'
export const DOING_TO_DONE = 'DOING_TO_DONE'

export const searchTodo = (value) => {
  return {
    type: SEARCH_TODO,
    payLoad: value
  }
}

export const filterTodo = (value) => {
  return {
    type: FILTER_TODO,
    payLoad: value
  }
}

export const fetchTodos = () => {
  return dispatch => {
    dispatch(fetchTodoBegin())
    return fetch('http://jsonplaceholder.typicode.com/todos')
      .then(res => res.json())
      .then(data => {
        dispatch(fetchTodoSuccess(data))
      })
      .catch(error => dispatch(fetchTodoError(error)))
  }
}

export const fetchTodoBegin = () => {
  return {
    type: FETCH_TODOS_BEGIN,
  }
}

export const fetchTodoSuccess = data => {
  return {
    type: FETCH_TODOS_SUCCESS,
    payLoad: data
  }
}
export const fetchTodoError = error => {
  return {
    type: FETCH_TODOS_ERROR,
    payLoad: error
  }
}

export const doingToDone = value => {
  return {
    type: DOING_TO_DONE,
    payLoad: value
  }
}


const initialState = {
  searchText: '',
  filterType: 'All',
  loading: false,
  data: [],
}

export const todoReducer = (state = initialState, action) => {
  switch (action.type) {
    case SEARCH_TODO:
      return {
        ...state,
        searchText: action.payLoad
      }
    case FILTER_TODO:
      return {
        ...state,
        filterType: action.payLoad
      }
    case FETCH_TODOS_BEGIN:
      return {
        ...state,
        loading: true
      }
    case FETCH_TODOS_SUCCESS:
      return {
        ...state,
        data: action.payLoad,
        loading: false,
        error: ''
      }
    case FETCH_TODOS_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payLoad
      }
    case DOING_TO_DONE:
      const newData = [...state.data]
      // action.payLoad  
      newData.forEach(todo => {
        if (action.payLoad == todo.id) {
          todo.completed = true
        }
      })
      return {
        ...state,
        data : newData
      }

    default:
      return state
  }
}