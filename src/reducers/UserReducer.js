export const FETCH_USERS_BEGIN = 'FETCH_USERS_BEGIN'
export const FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS'
export const FETCH_USERS_ERROR = 'FETCH_USERS_ERROR'
export const SEARCH_USER = 'SEARCH_USER'

export const fetchUsers = () => {
    return dispatch => {
        dispatch(fetchUserBegin())
        return fetch('https://jsonplaceholder.typicode.com/users')
            .then(res => res.json())
            .then(data => {
                dispatch(fetchUserSuccess(data))
            })
            .catch(error => dispatch(fetchUserError(error)))
    }
}

export const fetchUserBegin = () => {
    return {
        type: FETCH_USERS_BEGIN,
    }
}

export const fetchUserSuccess = data => {
    return {
        type: FETCH_USERS_SUCCESS,
        payLoad: data
    }
}
export const fetchUserError = error => {
    return {
        type: FETCH_USERS_ERROR,
        payLoad: error
    }
}

export const searchUser = (value) => {
    return {
        type: SEARCH_USER,
        payLoad: value
    }
}

const initialState = {
    loading: false,
    data: [],
    usersearchText :''
}

export const UserReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_USERS_BEGIN:
            return {
                ...state,
                loading: true
            }
        case FETCH_USERS_SUCCESS:
            return {
                data: action.payLoad,
                loading: false,
                error: ''
            }
        case FETCH_USERS_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payLoad
            }
        case SEARCH_USER:
            return {
                ...state,
                usersearchText: action.payLoad
            }
        default:
            return state
    }
}