import {combineReducers} from "redux";
import { todoReducer } from "./TodoReducer";
import { UserReducer } from "./UserReducer";


export const rootReducer = combineReducers({
    todosComp : todoReducer,
    usersComp : UserReducer
})